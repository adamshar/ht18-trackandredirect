import React, {Component} from 'react';
import {
  Button,
  Col,
  Form,
  FormGroup,
  FormText,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row
} from 'reactstrap';

class Modals extends Component {

  constructor(props) {
    super(props);
    this.state = {
      primary: false,
    };

    this.togglePrimary = this.togglePrimary.bind(this);

  }


  togglePrimary() {
    this.setState({
      primary: !this.state.primary,
    });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>

            <Button color="primary" onClick={this.togglePrimary} className="mr-1">New</Button>
            <Modal isOpen={this.state.primary} toggle={this.togglePrimary}
                   className={'modal-primary ' + this.props.className}>
              <ModalHeader toggle={this.togglePrimary}>New Order</ModalHeader>
              <ModalBody>
                <Form action="" method="post" className="form-horizontal">
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-ordering-user">Ordering User</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="fa fa-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="hf-ordering-user" name="hf-ordering-user" placeholder="Username" />
                      </InputGroup>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-ordering-user">Ordering User</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="hf-ordering-user" name="hf-ordering-user" placeholder="Enter Name..." />
                      <FormText className="help-block">Please enter your name</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-package-name">Package Name</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="hf-package-name" name="hf-package-name" placeholder="Enter Package Name..."/>
                      <FormText className="help-block">Please enter your package name</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-order-id">Order ID</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="hf-order-id" name="hf-order-id" placeholder="Enter Order ID..." />
                      <FormText className="help-block">Please enter your order ID</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-tracking-id">Tracking ID</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="hf-tracking-id" name="hf-tracking-id" placeholder="Enter Tracking ID..." />
                      <FormText className="help-block">Please enter your tracking ID</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="select">Select Delivery Company</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="select" name="select" id="select">
                        <option value="0">Please select</option>
                        <option value="1">Amazon</option>
                        <option value="2">Fedex</option>
                        <option value="3">UPS</option>
                        <option value="4">Other</option>
                      </Input>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="textarea-input">Description</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="textarea" name="textarea-input" id="textarea-input" rows="9"
                             placeholder="Anything we should know about your package?" />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-size">Estimated Size</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="hf-size" name="hf-size" placeholder="Enter Estimated Size..." />
                      <FormText className="help-block">Please enter your package estimated size</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="hf-weight">Estimated Weight</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="hf-weight" name="hf-weight" placeholder="Enter Estimated Weight..." />
                      <FormText className="help-block">Please enter your package estimated weight</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="date-input">Estimated Arrival Date</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="date" id="date-input" name="date-input" placeholder="date" />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label>Can be unboxed?</Label>
                    </Col>
                    <Col md="9">
                      <FormGroup check inline>
                        <Input className="form-check-input" type="radio" id="inline-radio1" name="inline-radios" value="option1" />
                        <Label className="form-check-label" check htmlFor="inline-radio1">Yes</Label>
                      </FormGroup>
                      <FormGroup check inline>
                        <Input className="form-check-input" type="radio" id="inline-radio2" name="inline-radios" value="option2" />
                        <Label className="form-check-label" check htmlFor="inline-radio2">No</Label>
                      </FormGroup>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="12">
                      <FormGroup check inline>
                        <Input className="form-check-input" type="checkbox" id="inline-checkbox1" name="inline-checkbox1" value="option1" />
                        <Label className="form-check-label" check htmlFor="inline-checkbox1">I Understand The Legal Waiver</Label>
                      </FormGroup>
                    </Col>
                  </FormGroup>

                </Form>

              </ModalBody>
              <ModalFooter>
                <Button color="primary" onClick={this.togglePrimary}>Submit</Button>{' '}
                <Button color="secondary" onClick={this.togglePrimary}>Cancel</Button>
              </ModalFooter>
            </Modal>


          </Col>
        </Row>
      </div>
    );
  }
}

export default Modals;
