import React, {Component} from 'react';
import {Card, CardBody, CardHeader, Col, Row} from 'reactstrap';

import OrdersTable, {OrdersPagination} from "./OrdersTable";
import Dashboard from "./DashBoard";
import NewOrderModal from "./NewOrderModal";

const mock_orders = [
  {orderedBy: "Igor", EstimatedArrivAldate: "2012/01/01",   status: "pending", id:0},
  {orderedBy: "Yaron", EstimatedArrivAldate: "2012/01/01",  status: "inactive",id:1},
  {orderedBy: "Maayan", EstimatedArrivAldate: "2012/01/01", status: "active",  id:3},
  {orderedBy: "Dudu", EstimatedArrivAldate: "2012/01/01",   status: "banned",  id:4}
];



class Tables extends Component {

  constructor(props) {
    super(props);
    this.state = {
      orders: []
    };
  }

  async componentDidMount() {
    const da = await fetch("https://jsonplaceholder.typicode.com/posts");
    const orders = await  da.json();
    this.setState({orders: mock_orders})
  }

  render() {
    return (
      <div className="animated fadeIn">

        <Dashboard/>

        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Track N Redirect
                <hr/>
                <NewOrderModal style={{float: "right"}} updateOrders={(newOrder) => {

                  let item = {
                    orderedBy: newOrder["ordering-user"],
                    EstimatedArrivAldate:newOrder["date-input"],
                    status: "pending",
                    id:0
                  };

                  this.state.orders.push(item);

                  this.setState({ orders: this.state.orders });

                }}/>
              </CardHeader>

              <CardBody>

                <OrdersTable orders={this.state.orders}/>
                <OrdersPagination/>

              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>

    );
  }
}

export default Tables;
