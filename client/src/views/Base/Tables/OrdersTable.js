import {Badge, Button, Pagination, PaginationItem, PaginationLink, Table} from "reactstrap";
import React from 'react';
import DetailsModal from "./DetailsModal";


const mock_orders = [
  {orderedBy: "Igor", EstimatedArrivAldate: "2012/01/01",   status: "pending", id:0},
  {orderedBy: "Yaron", EstimatedArrivAldate: "2012/01/01",  status: "inactive",id:1},
  {orderedBy: "Adam", EstimatedArrivAldate: "2012/01/01",   status: "pending", id:2},
  {orderedBy: "Maayan", EstimatedArrivAldate: "2012/01/01", status: "active",  id:3},
  {orderedBy: "Dudu", EstimatedArrivAldate: "2012/01/01",   status: "banned",  id:4}
];


const bannerColor = {
  banned: "danger",
  pending: "warning",
  inactive: "secondary",
  active: "success",
};


class OrdersTable extends React.Component {


  createOrders({orderedBy, EstimatedArrivAldate, status,id,idx}) {
    return <tr key={idx}>
      <td>{orderedBy}</td>
      <td>{EstimatedArrivAldate}</td>
      <td>Member</td>
      <td>
        <Badge color={bannerColor[status]}>{status}</Badge>
      </td>
      <td>
        <DetailsModal itemId={id}/>
      </td>
    </tr>;
  }

  render() {
    return <Table responsive striped>
      <thead>

      <tr>
        <th>Ordered by</th>
        <th>Estimated Arrival Date</th>
        <th>More</th>
        <th>Status</th>
        <th>Details</th>
      </tr>
      </thead>
      <tbody>

      {this.props.orders.map((order, i) => this.createOrders({...order, i}))}

      </tbody>

    </Table>;
  }
}

export const OrdersPagination = props => <Pagination>
  <PaginationItem disabled><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>
  <PaginationItem active>
    <PaginationLink tag="button">1</PaginationLink>
  </PaginationItem>
  <PaginationItem><PaginationLink tag="button">2</PaginationLink></PaginationItem>
  <PaginationItem><PaginationLink tag="button">3</PaginationLink></PaginationItem>
  <PaginationItem><PaginationLink tag="button">4</PaginationLink></PaginationItem>
  <PaginationItem><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>
</Pagination>


export default OrdersTable
