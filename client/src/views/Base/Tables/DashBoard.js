import React, { Component } from 'react';

import {CustomTooltips} from "@coreui/coreui-plugin-chartjs-custom-tooltips/dist/cjs/custom-tooltips";
import {getStyle} from "@coreui/coreui/dist/js/coreui-utilities";
import {Card, CardBody, Col, Row} from "reactstrap";
import {Line} from "react-chartjs-2";

const brandInfo = getStyle('--info');

const cardChartData2 = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
  datasets: [
    {
      label: 'count',
      backgroundColor: brandInfo,
      borderColor: 'rgba(255,255,255,.55)',
      data: [1, 18, 9, 17, 34, 22, 11],
    },
  ],
};



const cardChartOpts2 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent',
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent',
        },

      }],
    yAxes: [
      {
        display: false,
        ticks: {
          display: false,
          min: Math.min.apply(Math, cardChartData2.datasets[0].data) - 5,
          max: Math.max.apply(Math, cardChartData2.datasets[0].data) + 5,
        },
      }],
  },
  elements: {
    line: {
      tension: 0.00001,
      borderWidth: 1,
    },
    point: {
      radius: 4,
      hitRadius: 10,
      hoverRadius: 4,
    },
  },
};


const Dashboard = props => <Row>
  <Col xs="12" sm="6" lg="3">
    <Card className="text-white bg-info">
      <CardBody className="pb-0">

        <div className="text-value">15</div>
        <div>Awaiting pickup</div>
      </CardBody>
      <div className="chart-wrapper mx-3" style={{ height: '70px' }}>
        <Line data={cardChartData2} options={cardChartOpts2} height={70} />
      </div>
    </Card>
  </Col>

  <Col xs="12" sm="6" lg="3">

    <Card className="text-white bg-warning">
      <CardBody className="pb-0">

        <div className="text-value">6</div>
        <div>Picked up</div>
      </CardBody>
      <div className="chart-wrapper mx-3" style={{ height: '70px' }}>
        <Line data={cardChartData2} options={cardChartOpts2} height={70} />
      </div>
    </Card>
  </Col>

  <Col xs="12" sm="6" lg="3">

    <Card className="text-white bg-danger">
      <CardBody className="pb-0">

        <div className="text-value">8</div>
        <div>Delivered</div>
      </CardBody>
      <div className="chart-wrapper mx-3" style={{ height: '70px' }}>
        <Line data={cardChartData2} options={cardChartOpts2} height={70} />
      </div>
    </Card>
  </Col>
</Row>;

export default Dashboard
