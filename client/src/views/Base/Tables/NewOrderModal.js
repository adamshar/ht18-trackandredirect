import React, {Component} from 'react';
import {
  Button,
  Col,
  Form,
  FormGroup,
  FormText,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row
} from 'reactstrap';



class Modals extends Component {

  constructor(props) {
    super(props);
    this.state = {
      primary: false,
    };

    this.togglePrimary = this.togglePrimary.bind(this);
    this.onFormSubmit  = this.onFormSubmit.bind(this);

  }


  togglePrimary() {
    this.setState({
      primary: !this.state.primary,
    });
  }



  toJSON(form ) {
    let obj = {};
    let elements = form.querySelectorAll( "input, select, textarea" );
    for( let i = 0; i < elements.length; ++i ) {
      let element = elements[i];
      let name = element.name;
      let value = element.value;

      if( name ) {
        obj[ name ] = value;
      }
    }

    return obj
  }


  onFormSubmit = async e => {


    e.preventDefault();

    const form = e.target;


    this.props.updateOrders(this.toJSON(e.target));

    await fetch(form.action, {
      method: form.method,
      body: new FormData(e.target)
    });


    e.target.reset();

  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>

            <Button color="primary" onClick={this.togglePrimary} className="mr-1">New</Button>
            <Modal isOpen={this.state.primary} toggle={this.togglePrimary}
                   className={'modal-primary modal-lg ' + this.props.className || ""}>
              <ModalHeader toggle={this.togglePrimary}>New Order</ModalHeader>
              <ModalBody>
                <Form action="google.com/submit" method="post" className="form-horizontal" onSubmit={this.onFormSubmit}>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="ordering-user">Ordering User</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <InputGroup>
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="fa fa-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="text" id="ordering-user" name="ordering-user" placeholder="Username"/>
                      </InputGroup>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="package-name">Package Name</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="package-name" name="package-name"
                             placeholder="Enter Package Name..."/>
                      <FormText className="help-block">Please enter your package name</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="order-id">Order ID</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="order-id" name="order-id" placeholder="Enter Order ID..."/>
                      <FormText className="help-block">Please enter your order ID</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="tracking-id">Tracking ID</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="tracking-id" name="tracking-id" placeholder="Enter Tracking ID..."/>
                      <FormText className="help-block">Please enter your tracking ID</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="select">Select Delivery Company</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="select" name="delivered-to-ny-by" id="select">
                        <option value="Please">Please select</option>
                        <option value="Amazon">Amazon</option>
                        <option value="Fedex">Fedex</option>
                        <option value="UPS">UPS</option>
                      </Input>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="textarea-input">Description</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="textarea" name="textarea-input" id="textarea-input" rows="9"
                             placeholder="Anything we should know about your package?"/>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="size">Estimated Size</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="size" name="size" placeholder="Enter Estimated Size..."/>
                      <FormText className="help-block">Please enter your package estimated size</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="weight">Estimated Weight</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="weight" name="weight" placeholder="Enter Estimated Weight..."/>
                      <FormText className="help-block">Please enter your package estimated weight</FormText>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label htmlFor="date-input">Estimated Arrival Date</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="date" id="date-input" name="date-input" placeholder="date"/>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="3">
                      <Label>Can be unboxed?</Label>
                    </Col>
                    <Col md="9">
                      <FormGroup check inline>
                        <Input className="form-check-input" type="radio" id="inline-radio1" name="can-unboox"
                               value="yes"/>
                        <Label className="form-check-label" check htmlFor="inline-radio1">Yes</Label>
                      </FormGroup>
                      <FormGroup check inline>
                        <Input className="form-check-input" type="radio" id="inline-radio2" name="can-unboox"
                               value="no"/>
                        <Label className="form-check-label" check htmlFor="inline-radio2">No</Label>
                      </FormGroup>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="12">
                      <FormGroup check inline>
                        <Input className="form-check-input" type="checkbox" id="inline-checkbox1"
                               name="legal" value="yes"/>
                        <Label className="form-check-label" check htmlFor="inline-checkbox1">I Understand The Legal
                          Waiver</Label>
                      </FormGroup>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="1">
                      <Button color="primary" type={"submit"} onClick={this.togglePrimary}>Submit</Button>{' '}
                    </Col>
                    <Col md="1">
                      <Button color="secondary" onClick={this.togglePrimary}>Cancel</Button>
                    </Col>
                  </FormGroup>

                </Form>

              </ModalBody>
              <ModalFooter>

              </ModalFooter>
            </Modal>


          </Col>
        </Row>
      </div>
    );
  }
}

export default Modals;
