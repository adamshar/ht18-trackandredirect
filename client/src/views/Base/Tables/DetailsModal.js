import React, {Component} from 'react';
import {
  Button,
  Col,
  ListGroup,
  ListGroupItem,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Progress,
  Row
} from 'reactstrap';

class Modals extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal,
    });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>

            <Button onClick={this.toggle} className="mr-1">details</Button>
            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
              <ModalHeader toggle={this.toggle}>Package {this.props.itemId}</ModalHeader>
              <ModalBody>

                <Progress multi className="mb-3">
                  <Progress bar color="gray-300" value="25">Pending</Progress>
                  <Progress bar color="gray-400" value="25">Received</Progress>
                  <Progress bar color="gray-500" value="25">Picked up</Progress>
                  <Progress bar color="gray-600" value="25">Delivered</Progress>

                </Progress>


                <ListGroup>
                  <ListGroupItem>Package Name:</ListGroupItem>
                  <ListGroupItem>Delivery Status:</ListGroupItem>
                  <ListGroupItem>Estimated Size:</ListGroupItem>
                  <ListGroupItem>Estimated Weight:</ListGroupItem>
                  <ListGroupItem>Estimated Arrival Date:</ListGroupItem>
                  <ListGroupItem>Ordering Person Name:</ListGroupItem>
                  <ListGroupItem>Delivery Person Name:</ListGroupItem>
                </ListGroup>

              </ModalBody>
              <ModalFooter>
                <Button color="secondary" onClick={this.toggle}>Close</Button>
              </ModalFooter>
            </Modal>

          </Col>
        </Row>
      </div>
    );
  }
}

export default Modals;
