const usersData = [
  {id: 0, name: 'Zach Zigdon', startdate: '31-July-2018', enddate: '07-August-2018', destination: 'NY', flight: 'Delta DL 467'},
  {id: 1, name: 'Nir Ofry', startdate: '15-August-2018', enddate: '31-August-2018', destination: 'NY', flight: 'Delta DL 467'},
  {id: 2, name: 'Ronen Idrisov', startdate: '16-August-2018', enddate: '22-August-2018', destination: 'Israel', flight: 'Delta DL 468'},
  {id: 3, name: 'Yuval Pemper', startdate: '31-August-2018', enddate: '11-September-2018', destination: 'NY', flight: 'Delta DL 467'},
  {id: 4, name: 'Michal Livni', startdate: '15-September-2018', enddate: '21-September-2018', destination: 'NY', flight: 'El Al LY1'},
  {id: 5, name: 'Yaron Ben Zaken', startdate: '17-October-2018', enddate: '31-October-2018', destination: 'Israel', flight: 'Delta DL 468'},
  {id: 6, name: 'Tal Chalozin', startdate: '25-November-2018', enddate: '27-November-2018', destination: 'Israel', flight: 'El Al LY2'},
  {id: 7, name: 'Zach Zigdon', startdate: '02-December-2018', enddate: '10-December-2018', destination: 'London', flight: 'British BA165'}
];

export default usersData
