import React, {Component} from 'react';
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  ListGroup,
  ListGroupItem,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Progress,
  Row
} from 'reactstrap';

const pending_color = {'Pending': "pending-color",'Received': "none-color",'Picked': "none-color",'Delivered': "none-color"};
const received_color = {'Pending': "none-color",'Received': "received-color",'Picked': "none-color",'Delivered': "none-color"};
const picked_color = {'Pending': "none-color",'Received': "none-color",'Picked': "picked-color",'Delivered': "none-color"};
const delivered_color = {'Pending': "none-color",'Received': "none-color",'Picked': "none-color",'Delivered': "delivered-color"};

class Modals extends Component {

  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal,
    });
  }

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col>

            <Button onClick={this.toggle} className="mr-1 details-btn">Details</Button>
            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
              <ModalHeader toggle={this.toggle}>{this.props.itemOrderedBy}'s Package</ModalHeader>
              <ModalBody>

                <Progress multi className="mb-3">
                  <Progress bar className={pending_color[this.props.itemStatus]} value="25">Pending</Progress>
                  <Progress bar className={received_color[this.props.itemStatus]} value="25">Received</Progress>
                  <Progress bar className={picked_color[this.props.itemStatus]} value="25">Picked</Progress>
                  <Progress bar className={delivered_color[this.props.itemStatus]} value="25">Delivered</Progress>

                </Progress>


                <ListGroup>
                  <ListGroupItem>Package Name</ListGroupItem>
                  <ListGroupItem><span>Delivery Status</span>{this.props.itemStatus}</ListGroupItem>
                  <ListGroupItem><span>Estimated Size</span></ListGroupItem>
                  <ListGroupItem><span>Estimated Weight</span></ListGroupItem>
                  <ListGroupItem><span>Estimated Arrival Date</span>{this.props.itemArrivalDate}</ListGroupItem>
                  <ListGroupItem><span>Ordering Person Name</span>{this.props.itemOrderedBy}</ListGroupItem>
                  <ListGroupItem><span>Delivery Person Name</span></ListGroupItem>
                </ListGroup>

              </ModalBody>
              <ModalFooter>
                <Button color="secondary" onClick={this.toggle}>Close</Button>
              </ModalFooter>
            </Modal>

          </Col>
        </Row>
      </div>
    );
  }
}

export default Modals;
