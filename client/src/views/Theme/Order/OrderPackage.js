import React, {Component} from 'react';
import {Card, CardBody, CardHeader, Col, Row} from 'reactstrap';

import OrdersTable, {OrdersPagination} from "./OrdersTable";
import Dashboard from "./DashBoard";
import NewOrderModal from "./NewOrderModal";


const bannerColor = {
  Ordered: "pending-color",
  "In Progress": "received-color",
  "Picked Up": "picked-color",
  Delivered: "delivered-color"
};


class OrderPackage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      orders: []
    };
  }

  componentDidMount() {

      setInterval(async() => {
        const da = await fetch("/track");
        try {
        const orders = await  da.json();

        const bla = orders.map(order =>
          ({
            orderedBy: order['orderingPersonName'],
            EstimatedArrivAldate: order['arrivalDate'],
            status: order['status'],
            id: order['orderId']
          })
        );

        this.setState({orders: bla})
        } catch (e) {}
      }, 500)
    
  }

  render() {
    return (
      <div className="animated fadeIn">
        <div className="header-bg home-wrapper" xs="12" lg="12" id="dashboard">

          <Dashboard/>
        </div>
        <div className="home-wrapper">
          <Row>
            <Col xs="12" lg="12">
              <Card>
                <CardHeader>
                  <i className="fa fa-align-justify"></i>Packages
                  <hr/>
                  <NewOrderModal style={{float: "right"}} updateOrders={(newOrder) => {


                  let item = {
                    orderedBy: newOrder["ordering-user"],
                    EstimatedArrivAldate:newOrder["date-input"],
                    status: "Ordered",
                    id:0
                  };

                  this.state.orders.push(item);

                  this.setState({ orders: this.state.orders });

                }}/>
                </CardHeader>

                <CardBody>

                  <OrdersTable orders={this.state.orders}/>
                  <OrdersPagination/>

                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      </div>

    );
  }
}

export default OrderPackage;
