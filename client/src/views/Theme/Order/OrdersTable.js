import {Badge, Pagination, PaginationItem, PaginationLink, Table} from "reactstrap";
import React from 'react';
import DetailsModal from "./DetailsModal";


const bannerColor = {
  Ordered: "secondary",
  "In Progress": "success",
  "Picked Up": "primary",
  Delivered: "warning"
};


class OrdersTable extends React.Component {

  createOrders({orderedBy, EstimatedArrivAldate, status,id,idx}) {
    return <tr key={idx}>
      <td>{orderedBy}</td>
      <td>{EstimatedArrivAldate}</td>
      <td>Member</td>
      <td>
        <Badge color={bannerColor[status]}>{status}</Badge>
      </td>
      <td>
        <DetailsModal itemId={id} itemStatus={status} itemArrivalDate={EstimatedArrivAldate} itemOrderedBy={orderedBy}/>
      </td>
    </tr>;
  }

  render() {
    return <Table responsive>
      <thead>

      <tr>
        <th>Ordered by</th>
        <th>Estimated Arrival Date</th>
        <th>More</th>
        <th>Status</th>
        <th>Details</th>
      </tr>
      </thead>
      <tbody>

      {this.props.orders.map((order, i) => this.createOrders({...order, i}))}

      </tbody>

    </Table>;
  }
}

export const OrdersPagination = props => <Pagination>
  <PaginationItem disabled><PaginationLink previous tag="button">Prev</PaginationLink></PaginationItem>
  <PaginationItem active>
    <PaginationLink tag="button">1</PaginationLink>
  </PaginationItem>
  <PaginationItem className="picked-color"><PaginationLink tag="button">2</PaginationLink></PaginationItem>
  <PaginationItem className="picked-color"><PaginationLink tag="button">3</PaginationLink></PaginationItem>
  <PaginationItem className="picked-color"><PaginationLink tag="button">4</PaginationLink></PaginationItem>
  <PaginationItem className="picked-color"><PaginationLink next tag="button">Next</PaginationLink></PaginationItem>
</Pagination>


export default OrdersTable
