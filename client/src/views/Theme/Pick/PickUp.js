import React, {Component} from 'react';
import {Card, CardBody, CardHeader, Col, Row} from 'reactstrap';



const mock_orders = [
  {orderedBy: "Igor", EstimatedArrivAldate: "2012/01/01",   status: "pending", id:0},
  {orderedBy: "Yaron", EstimatedArrivAldate: "2012/01/01",  status: "inactive",id:1},
  {orderedBy: "Maayan", EstimatedArrivAldate: "2012/01/01", status: "active",  id:3},
  {orderedBy: "Dudu", EstimatedArrivAldate: "2012/01/01",   status: "banned",  id:4}
];



class PickUp extends Component {

  constructor(props) {
    super(props);
    this.state = {
      orders: []
    };
  }

  async componentDidMount() {
    const da = await fetch("https://jsonplaceholder.typicode.com/posts");
    const orders = await  da.json();
    this.setState({orders: mock_orders})
  }

  render() {
    return (
      <div className="animated fadeIn">


        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Order
                <hr/>

              </CardHeader>

              <CardBody>

                <h1>Hello and thanks for all the fish!</h1>

              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>

    );
  }
}

export default PickUp;
