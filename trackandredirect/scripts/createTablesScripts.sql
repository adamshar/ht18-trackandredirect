CREATE TABLE htUser(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  slackId VARCHAR(128));

CREATE TABLE htPackage(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name VARCHAR(255) NOT NULL,
  orderId VARCHAR(255) NOT NULL,
  trackingId VARCHAR(255) NOT NULL,
  orderingUser INTEGER,
  deliveryCompany VARCHAR(255) NOT NULL,
  contentDescription VARCHAR(2048),
  estimatedSizeW INTEGER,
  estimatedSizeH INTEGER,
  estimatedSizeD INTEGER,
  estimatedWeight DOUBLE,
  arrivalDate DATE,
  canBeUnboxed BOOLEAN,
  additionalInfo TEXT,
  CONSTRAINT fk_orderingUer FOREIGN KEY (orderingUser) REFERENCES htUser(id)
);

CREATE TABLE htPackageState (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  state NVARCHAR(128) NOT NULL
);

CREATE TABLE htPackageStatus(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  packageId INTEGER,
  status INTEGER,
  assigned BOOLEAN,
  assignee INTEGER,
  CONSTRAINT fk_packageId FOREIGN KEY (packageId) REFERENCES htPackage(id),
  CONSTRAINT fk_status FOREIGN KEY (status) REFERENCES htPackageState(id),
  constraint fk_assignee FOREIGN KEY (assignee) REFERENCES htUser(id)
);
