INSERT INTO htUser (id, name, email, slackId) VALUES
  (1, 'Ron Zionpour', 'ron@innovid.com', 'ron'),
   (2, 'Yuval Pemper', 'yuval@innovid.com', 'yuval'),
   (3, 'Julia Greene', 'julia@innovid.com', 'julia-greene');

INSERT INTO htPackageState (id, state) VALUES
  (1, 'Ordered'),
  (2, 'In Storage'),
  (3, 'Picked Up'),
  (4, 'Delivered'),
  (5, 'Canceled');

INSERT INTO htPackage (id, name, orderId, trackingId, orderingUser, deliveryCompany, contentDescription, estimatedSizeW, estimatedSizeH, estimatedSizeD, estimatedWeight, arrivalDate, canBeUnboxed, additionalInfo) VALUES
  (1, 'Wife order from Carters', '113-1179097-9722611', 'LX26672419', 1, 'Amazon', 'Cloth for the kids', 15, 20, 10, 1.5, '2018-07-25', 'false', '');

INSERT INTO htPackage (id, name, orderId, trackingId, orderingUser, deliveryCompany, contentDescription, estimatedSizeW, estimatedSizeH, estimatedSizeD, estimatedWeight, arrivalDate, canBeUnboxed, additionalInfo) VALUES
  (2, 'Water Bottle', '7290002331018', '', 1, 'Amazon', 'Kosher Water Bottle', 2, 20, 5, 1.5, '2018-07-24', 'false', 'Additional Info');

INSERT INTO htPackageStatus (id, packageId, status, assigned, assignee) VALUES
  (1, 1, 1, 'false', NULL);

INSERT INTO htPackageStatus (id, packageId, status, assigned, assignee) VALUES
  (2, 2, 1, 'false', NULL);