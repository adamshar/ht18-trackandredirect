package model;

public class UpdateOrder {
    private String trackingId;
    private String orderState;
    private Boolean isAssigned;
    private String assigneeSlackName;

    public String getTrackingId() {
        return trackingId;
    }

    public String getOrderState() {
        return orderState;
    }

    public Boolean getAssigned() {
        return isAssigned;
    }

    public String getAssignedAsString() {
        if (isAssigned) {
            return "\'" + "true" + "\'";
        } else {
            return "\'" + "false" + "\'";
        }
    }

    public String getAssigneeSlackName() {
        return assigneeSlackName;
    }
}
