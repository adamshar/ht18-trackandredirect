package model;

public class OrderInfo {
    public String name;
    private String trackingId;
    public String status;
    public int size;
    public double weight;
    public String arrivalDate;
    private final int orderingPersonId;
    private final int assigneeId;
    public String orderingPersonName;
    public String deliveryPersonName;

    public OrderInfo(String name, String trackingId, String status, int size, double weight, String arrivalDate, int orderingPersonId, int assigneeId) {
        this.name = name;
        this.trackingId = trackingId;
        this.status = status;
        this.size = size;
        this.weight = weight;
        this.arrivalDate = arrivalDate;
        this.orderingPersonId = orderingPersonId;
        this.assigneeId = assigneeId;
    }

    public void setOrderingPersonName(String orderingPersonName) {
        this.orderingPersonName = orderingPersonName;
    }

    public void setDeliveryPersonName(String deliveryPersonName) {
        this.deliveryPersonName = deliveryPersonName;
    }

    public int getOrderingPersonId() {
        return orderingPersonId;
    }

    public int getAssigneeId() {
        return assigneeId;
    }
}
