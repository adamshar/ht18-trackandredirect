package model;

public enum OrderState {
    ORDERED(1, "Ordered"), IN_STORAGE(2, "In Progress"), PICKED_UP(3, "Picked Up"), DELIVERED(4, "Delivered"), CANCELED(5, "Canceled");

    private final int state;
    private final String displayName;

    OrderState (int id, String displayName) {
        this.state = id;
        this.displayName = displayName;
    }

    public int getStateIntValue() {
        return state;
    }
    public String getDisplayName() { return displayName; }
}
