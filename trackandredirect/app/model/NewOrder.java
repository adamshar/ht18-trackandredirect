package model;

public class NewOrder {
    public String  name;
    public String  orderId;
    public String  trackingId;
    public String  orderingUser;
    public String  deliveryCompany;
    public String  contentDescription;
    public int     estimatedSize;
    public double  estimatedWeight;
    public String  arrivalDate;
    public Boolean canBeUnboxed;
    public String  additionalInfo;
}
