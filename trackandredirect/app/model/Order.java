
package model;

public class Order {
    private final String trackingId;
    private final String orderState;
    private final String orderName;
    private final String contentDescription;
    private final int orderer;
    private final int assignee;
    private String ordererName;
    private String assigneeName;
    private String ordererSlackId;
    private String assigneeSlackId;

    public Order(String trackingId, String orderState, String orderName, String contentDescription, int orderer, int assignee) {
        this.trackingId = trackingId;
        this.orderState = orderState;
        this.orderName = orderName;
        this.contentDescription = contentDescription;
        this.orderer = orderer;
        this.assignee = assignee;

        ordererName = "";
        ordererSlackId = "";
        assigneeName = "";
        assigneeSlackId = "";
    }

    public String getTrackingId() {
        return trackingId;
    }

    public String getOrderState() {
        return orderState;
    }

    public String getOrderName() {
        return orderName;
    }

    public String getContentDescription() {
        return contentDescription;
    }

    public int getOrdererId() {
        return orderer;
    }

    public int getAssigneeId() {
        return assignee;
    }

    public void setOrdererName(String ordererName) {
        this.ordererName = ordererName;
    }

    public void setAssigneeName(String assigneeName) {
        this.assigneeName = assigneeName;
    }

    public void setOrdererSlackId(String ordererSlackId) {
        this.ordererSlackId = ordererSlackId;
    }

    public void setAssigneeSlackId(String assigneeSlackId) {
        this.assigneeSlackId = assigneeSlackId;
    }
}
