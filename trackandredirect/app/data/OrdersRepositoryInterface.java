package data;

import com.google.inject.ImplementedBy;
import model.NewOrder;
import model.Order;
import model.OrderInfo;
import model.UpdateOrder;

import java.sql.SQLException;
import java.util.List;

@ImplementedBy(OrdersRepository.class)
public interface OrdersRepositoryInterface {
    void updateOrderToReceived(String trackingId) throws SQLException;

    List<Order> getOrdersBySlackUsername(String slackUsername) throws SQLException;

    void addNewOrder(NewOrder newOrder) throws SQLException;

    List<OrderInfo> getAllOrders() throws SQLException;

    void updateOrderState(UpdateOrder updateOrder) throws Exception;

    String getPackageNameFromTrackingId(String trackingId) throws SQLException;

    String getUserNameBySlackName(String slackName) throws SQLException;
}
