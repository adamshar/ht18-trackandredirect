package data;

import model.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OrdersRepository implements OrdersRepositoryInterface{
    public OrdersRepository() {
    }

    @Override
    public void updateOrderToReceived(String trackingId) throws SQLException {
        int packageId = getPackageIdFromTrackingId(trackingId);
            OrderState orderState = getOrderStateFromPackageId(packageId);

            if (orderState.equals(OrderState.ORDERED)) {
                updateOrderStateToReceived(packageId);
            }
    }

    @Override
    public List<Order> getOrdersBySlackUsername(String slackUsername) throws SQLException {
        int userId = getUserIdBySlackName(slackUsername);
        List<Order> orders = getOrdersByUserId(userId);

        return orders;
    }

    @Override
    public void addNewOrder(NewOrder newOrder) throws SQLException {
        int userId = getUserIdBySlackName(newOrder.orderingUser);
        String insertNewOrderStatement = "INSERT INTO htPackage (name, orderId, trackingId, orderingUser, deliveryCompany, contentDescription, estimatedSizeW, estimatedSizeH, estimatedSizeD, estimatedWeight, arrivalDate, canBeUnboxed, additionalInfo)" +
                " VALUES\n" +
                " (" +
                "\'" + newOrder.name + "\', " +
                "\'" + newOrder.orderId + "\', " +
                "\'" + newOrder.trackingId + "\', " +
                userId + ", " +
                "\'" + newOrder.deliveryCompany + "\', " +
                "\'" + newOrder.contentDescription + "\', " +
                newOrder.estimatedSize + ", " +
                1 + ", " +
                2 + ", " +
                newOrder.estimatedWeight + ", " +
                "\'" + newOrder.arrivalDate + "\', " +
                "\'" + newOrder.canBeUnboxed + "\', " +
                "\'" + newOrder.additionalInfo + "\')";

        executeUpdate(insertNewOrderStatement);

        int packageId = getPackageIdFromTrackingId(newOrder.trackingId);
        String newPackageStatusStatement = "INSERT INTO htPackageStatus (packageId, status, assigned, assignee) VALUES\n" +
                "(" + packageId + ", " + OrderState.ORDERED.getStateIntValue() + ", 'false', NULL)";

        executeUpdate(newPackageStatusStatement);
    }

    @Override
    public List<OrderInfo> getAllOrders() throws SQLException {
        List<OrderInfo> ordersInfo = new ArrayList<>();

        String statementStr = "SELECT name, status, trackingId, estimatedSizeW, arrivalDate, estimatedWeight, orderingUser, assignee FROM htPackageStatus INNER JOIN htPackage on htPackageStatus.packageId = htPackage.id\n" +
                "GROUP BY packageId ORDER BY htPackageStatus.id";

        ResultSet rs = executeStatement(statementStr);

        while (rs.next()) {
            String name = rs.getString("name");
            OrderState state = getOrderStateFromInt(rs.getInt("status"));
            String trackingId = rs.getString("trackingId");
            int estimatedSize = rs.getInt("estimatedSizeW");
            double weight = rs.getDouble("estimatedWeight");
            String arrivalDate = rs.getString("arrivalDate");
            int orderingUser = rs.getInt("orderingUser");
            int assignee = rs.getInt("assignee");

            ordersInfo.add(new OrderInfo(name, trackingId, state.getDisplayName(), estimatedSize, weight, arrivalDate, orderingUser, assignee));
        }

        rs.close();

        // Add usernames
        for (OrderInfo orderInfo: ordersInfo) {
            orderInfo.setOrderingPersonName(getUserNameById(orderInfo.getOrderingPersonId()));

            if (orderInfo.getOrderingPersonId() > 0) {
                orderInfo.setDeliveryPersonName(getUserNameById(orderInfo.getAssigneeId()));
            }
        }

        return ordersInfo;
    }

    @Override
    public void updateOrderState(UpdateOrder updateOrder) throws Exception {
        int packageId = getPackageIdFromTrackingId(updateOrder.getTrackingId());
        int status = getOrderStateFromString(updateOrder.getOrderState()).getStateIntValue();
        int assigneeId = getUserIdBySlackName(updateOrder.getAssigneeSlackName());
        String assignee = assigneeId > 0 ? Integer.toString(assigneeId) : "NULL";

        String insertStatement = "INSERT INTO htPackageStatus (packageId, status, assigned, assignee) VALUES " +
                "(" + packageId + ", " + status + ", " + updateOrder.getAssignedAsString() + ", " + assignee + ");";

        if (packageId > 0) {
            executeUpdate(insertStatement);
        } else {
            throw new Exception("Package not found. Tracking ID: " + updateOrder.getTrackingId());
        }
    }

    @Override
    public String getPackageNameFromTrackingId(String trackingId) throws SQLException {
        String packageName = "";

        String statementStr = "SELECT name FROM htPackage WHERE trackingId = " + "\"" + trackingId + "\"" + " LIMIT 1";
        ResultSet orderRow = executeStatement(statementStr);

        while (orderRow.next()) {
            packageName =  orderRow.getString("name");
        }

        orderRow.close();

        return packageName;
    }

    @Override
    public String getUserNameBySlackName(String slackName) throws SQLException {
        String username = "";

        String statementStr = "SELECT name FROM htUser WHERE slackId = \'" + slackName + "\';";
        ResultSet rs = executeStatement(statementStr);

        while (rs.next()) {
            username = rs.getString("name");
            break;
        }

        rs.close();

        return username;
    }

    private OrderState getOrderStateFromString(String orderStateStr) {
        OrderState orderStateRet = null;

        for (OrderState orderState : OrderState.values()) {
            if (orderState.getDisplayName().equalsIgnoreCase(orderStateStr)) {
                orderStateRet = orderState;
                break;
            }
        }

        return orderStateRet;
    }

    private List<Order> getOrdersByUserId(int userId) throws SQLException {
        List<Order> orders = new ArrayList<>();
        List<Integer> packageIds = new ArrayList<>();

        String statementStr = "SELECT * FROM htPackageStatus INNER JOIN htPackage on htPackageStatus.packageId = htPackage.id " +
                "WHERE packageId in (SELECT id FROM htPackage WHERE orderingUser = " + userId + ") GROUP BY packageId ORDER BY id";

        ResultSet rs = executeStatement(statementStr);

        while (rs.next()) {
            String trackingId = rs.getString("trackingId");
            String orderState = getOrderStateFromInt(rs.getInt("status")).getDisplayName();
            String name = rs.getString("name");
            String contentDesc = rs.getString("contentDescription");
            int assigneeId = rs.getInt("assignee");

            orders.add(new Order(trackingId, orderState, name, contentDesc, userId, assigneeId));
        }

        rs.close();

        // Add usernames
        for (Order order: orders) {
            order.setOrdererName(getUserNameById(order.getOrdererId()));
            order.setOrdererSlackId(getSlackNameByUserId(order.getOrdererId()));

            if (order.getAssigneeId() > 0) {
                order.setAssigneeName(getUserNameById(order.getAssigneeId()));
                order.setAssigneeSlackId(getSlackNameByUserId(order.getAssigneeId()));
            }
        }

        return orders;
    }

    private String getUserNameById(int userID) throws SQLException {
        String userName = "";

        String statementStr = "SELECT name from htUser WHERE id = " + userID;
        ResultSet rs = executeStatement(statementStr);

        while (rs.next()) {
            userName = rs.getString("name");
            break;
        }

        rs.close();

        return userName;
    }

    private int getUserIdBySlackName(String slackUsername) throws SQLException {
        int userId = -1;

        String statementStr = "SELECT * FROM htUser WHERE slackId = " + "\'" + slackUsername + "\'";
        ResultSet rs = executeStatement(statementStr);

        while (rs.next()) {
            userId = rs.getInt("id");
        }

        rs.close();

        return userId;
    }

    private String getSlackNameByUserId(int userId) throws SQLException {
        String slackName = "";

        String statementStr = "SELECT * FROM htUser WHERE id = " + userId;
        ResultSet rs = executeStatement(statementStr);

        while (rs.next()) {
            slackName = rs.getString("slackId");
            break;
        }

        rs.close();

        return slackName;
    }

    private void updateOrderStateToReceived(int packageId) throws SQLException {
        String statementStr = "INSERT INTO htPackageStatus (packageId, status, assigned, assignee) VALUES \n" +
                "  (" + packageId + ", " + OrderState.IN_STORAGE.getStateIntValue() + ", 'false', NULL)";

        executeUpdate(statementStr);

    }

    private void executeUpdate(String statementStr) throws SQLException {
        Connection connection;

        connection = DriverManager.getConnection("jdbc:sqlite:trackDb.db");
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);  // set timeout to 30 sec.

        statement.executeUpdate(statementStr);

        connection.close();
    }

    private OrderState getOrderStateFromPackageId(int packageId) throws SQLException {
        OrderState orderStateRet;
        int state = -1;

        String statementStr = "SELECT * FROM htPackageStatus WHERE packageId = " + packageId + " ORDER BY id DESC LIMIT 1;";
        ResultSet rs = executeStatement(statementStr);

        while (rs.next()) {
            state = rs.getInt("status");
            break;
        }

        rs.close();

        orderStateRet = getOrderStateFromInt(state);

        return orderStateRet;
    }

    private OrderState getOrderStateFromInt(int state) {
        OrderState orderStateRet = null;

        for (OrderState orderState : OrderState.values()) {
            if (orderState.getStateIntValue() == state) {
                orderStateRet = orderState;
                break;
            }
        }

        return orderStateRet;
    }

    private int getPackageIdFromTrackingId(String orderId) throws SQLException {
        int packageId = -1;

        String statementStr = "SELECT id FROM htPackage WHERE trackingId = " + "\"" + orderId + "\"" + " LIMIT 1";
        ResultSet orderRow = executeStatement(statementStr);

        while (orderRow.next()) {
            packageId =  orderRow.getInt("id");
        }

        orderRow.close();

        return packageId;
    }

    private ResultSet executeStatement(String statementStr) throws SQLException {
        Connection connection = null;

        connection = DriverManager.getConnection("jdbc:sqlite:trackDb.db");
        Statement statement = connection.createStatement();
        statement.setQueryTimeout(30);  // set timeout to 30 sec.

        ResultSet rs = statement.executeQuery(statementStr);
        return rs;
    }
}
