package data;

import org.apache.commons.lang.text.StrSubstitutor;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.TimeUnit;

public class SlackProxy implements SlackProxyInterface {

    private WSClient wsClient;

    @Inject
    public SlackProxy(WSClient wsClient) {
        this.wsClient = wsClient;
    }

    String slackAddress = "https://hooks.slack.com/services/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\n";
    String orderDeliveredTemplateMessage = "{\n" +
            "           \"username\": \"piggyback\",\n" +
            "           \"channel\": \"@dudu\",\n" +
            "           \"link_names\": true,\n" +
            "           \"attachments\": [\n" +
            "           {\n" +
            "         \"fallback\": \"${PackageName} was delivered successfully\",\n" +
            "         \"color\": \"#FF8000\",\n" +
            "         \"text\": \"${PackageName} was delivered successfully\",\n" +
            "         \"image_url\": \"https://media1.tenor.com/images/18c3e13de4efe4b7cf52de30686c6ba6/tenor.gif?itemid=5025871\n" +
            "https://media1.tenor.com/images/18c3e13de4efe4b7cf52de30686c6ba6/tenor.gif?itemid=5025871\n" +
            "\"\n" +
            "           },\n" +
            "            {\n" +
            "               \"fallback\": \"You are unable to choose a game\",\n" +
            "               \"callback_id\": \"thanksyoujulia\",\n" +
            "               \"color\": \"#3AA3E3\",\n" +
            "               \"attachment_type\": \"default\",\n" +
            "               \"actions\": [\n" +
            "                   {\n" +
            "                       \"name\": \"Thanks\",\n" +
            "                       \"text\": \"Send thank you note to Julia\",\n" +
            "                       \"type\": \"button\",\n" +
            "                       \"value\": \"Thanks\"\n" +
            "                   }\n" +
            "                 \n" +
            "               ]\n" +
            "           }        \n" +
            "       ]\n" +
            "   }";

    String statusChangeMessage = "{\n" +
            "           \"username\": \"piggyback\",\n" +
            "           \"channel\": \"@dudu\",\n" +
            "           \"link_names\": true,\n" +
            "           \"attachments\": [\n" +
            "           {\n" +
            "         \"fallback\": \"${PackageName} was picked-up successfully by ${assigneeName}\",\n" +
            "         \"color\": \"#FF8000\",\n" +
            "         \"text\": \"${PackageName} was picked-up successfully by ${assigneeName}\",\n" +
            "         \"image_url\": \"https://media.tenor.com/images/9232594d031e00ae0ce489642ea90d03/tenor.gif\n" +
            "\"\n" +
            "           },\n" +
            "            {\n" +
            "               \"fallback\": \"You are unable to choose a game\",\n" +
            "               \"callback_id\": \"thanksyoujulia\",\n" +
            "               \"color\": \"#3AA3E3\",\n" +
            "               \"attachment_type\": \"default\",\n" +
            "               \"actions\": [\n" +
            "                   {\n" +
            "                       \"name\": \"Thanks\",\n" +
            "                       \"text\": \"Send thank you note to Julia\",\n" +
            "                       \"type\": \"button\",\n" +
            "                       \"value\": \"Thanks\"\n" +
            "                   }\n" +
            "                 \n" +
            "               ]\n" +
            "           }        \n" +
            "       ]\n" +
            "   }";


    public void sendOrderArrivedMessage(String packageName) throws Exception {
        Map<String, String> valuesMap = new HashMap<>();
        valuesMap.put("PackageName", packageName);
        StrSubstitutor sub = new StrSubstitutor(valuesMap);

        String resolvedString = sub.replace(orderDeliveredTemplateMessage);

        SendSlackRequest(slackAddress, resolvedString);
    }

    public void sendPickUpMessage(String packageName, String assigneeName) throws Exception {
        Map<String, String> valuesMap = new HashMap<>();
        valuesMap.put("PackageName", packageName);
        valuesMap.put("assigneeName", assigneeName);
        StrSubstitutor sub = new StrSubstitutor(valuesMap);

        String resolvedString = sub.replace(orderDeliveredTemplateMessage);

        SendSlackRequest(slackAddress, resolvedString);
    }

    private void SendSlackRequest(String slackAddress, String templateString) throws Exception {
        CompletionStage<WSResponse> post = wsClient.url(slackAddress).addHeader("Content-Type", "application/json").post(templateString);

        post.toCompletableFuture().get(5000, TimeUnit.MILLISECONDS);
    }
}
