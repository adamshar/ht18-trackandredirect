package data;

import com.google.inject.ImplementedBy;

@ImplementedBy(SlackProxy.class)
public interface SlackProxyInterface {
    void sendOrderArrivedMessage(String packageName) throws Exception;

    void sendPickUpMessage(String packageName, String assigneeName) throws Exception;
}
