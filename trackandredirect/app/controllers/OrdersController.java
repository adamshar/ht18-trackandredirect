package controllers;

import com.google.gson.Gson;
import data.OrdersRepositoryInterface;
import data.SlackProxyInterface;
import model.NewOrder;
import model.OrderInfo;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.sql.SQLException;
import java.util.List;

public class OrdersController extends Controller {
    private OrdersRepositoryInterface ordersRepo;
    private SlackProxyInterface slackProxy;

    @Inject
        public OrdersController(OrdersRepositoryInterface ordersRepo, SlackProxyInterface slackProxy) {
        this.ordersRepo = ordersRepo;
        this.slackProxy = slackProxy;
    }

    public Result orderedRecieved(String trackingId) {
        try {
            System.out.println("Recieved Order ID: "+ trackingId);
            ordersRepo.updateOrderToReceived(trackingId);
            String packageName = ordersRepo.getPackageNameFromTrackingId(trackingId);

            slackProxy.sendOrderArrivedMessage(packageName);
        } catch (Exception e) {
            System.out.println("Got exception: " + e.getMessage());
            return internalServerError(e.getMessage());
        }

        return ok();
    }

    public Result submitNew() {
        try {
            System.out.println("Submit new...");
            String jsonText = request().body().asJson().toString();

            System.out.println("Submit new JSON: " + jsonText);

            NewOrder newOrder = new Gson().fromJson(jsonText, NewOrder.class);
            ordersRepo.addNewOrder(newOrder);
        } catch (SQLException e) {
            System.out.println("Got exception: " + e.getMessage());
            return internalServerError(e.getMessage());
        }

        return ok();
    }

    public Result getAllOrders() {
        try {
            System.out.println("Getting all orders...");
            List<OrderInfo> allOrders = ordersRepo.getAllOrders();

            String json = new Gson().toJson(allOrders);

            return ok(json);

        } catch (SQLException e) {
            System.out.println("Got exception: " + e.getMessage());
            return internalServerError(e.getMessage());
        }
    }
}
