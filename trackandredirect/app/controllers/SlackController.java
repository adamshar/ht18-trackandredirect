package controllers;

import com.google.gson.Gson;
import data.OrdersRepositoryInterface;
import data.SlackProxyInterface;
import model.Order;
import model.OrderState;
import model.UpdateOrder;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.sql.SQLException;
import java.util.List;

public class SlackController extends Controller {
    private OrdersRepositoryInterface ordersRepo;
    private SlackProxyInterface slackProxy;

    @Inject
    public SlackController(OrdersRepositoryInterface ordersRepo, SlackProxyInterface slackProxy) {
        this.ordersRepo = ordersRepo;
        this.slackProxy = slackProxy;
    }

    public Result getUserOrders(String slackUsername) throws SQLException {
        System.out.println("Get user orders for Slack. User name:" + slackUsername);

        List<Order> orders = ordersRepo.getOrdersBySlackUsername(slackUsername);

        String json = new Gson().toJson(orders);

        return ok(json);
    }

    public Result update() {
        try {
            System.out.println("Updating order...");
            String jsonText = request().body().asText();

            System.out.println("Update new JSON: " + jsonText);

            UpdateOrder updateOrder = new Gson().fromJson(jsonText, UpdateOrder.class);
            ordersRepo.updateOrderState(updateOrder);

            if (OrderState.PICKED_UP.getDisplayName().equalsIgnoreCase(updateOrder.getOrderState())) {
                String packageName = ordersRepo.getPackageNameFromTrackingId(updateOrder.getTrackingId());
                String assigneeName = ordersRepo.getUserNameBySlackName(updateOrder.getAssigneeSlackName());

                slackProxy.sendPickUpMessage(packageName, assigneeName);
            }

            return ok();
        } catch (Exception e) {
            System.out.println("Got exception: " + e.getMessage());
            return internalServerError(e.getMessage());
        }
    }
}
