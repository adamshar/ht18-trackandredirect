name := """trackAndRedirect"""
organization := "com.innovid"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
  guice,
  ws,
  "org.xerial" % "sqlite-jdbc" % "3.23.1",
  "com.google.code.gson" % "gson" % "2.8.5",
  "commons-lang" % "commons-lang" % "2.6"
)
